package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Vector;

import json.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;


/**
 * Servlet implementation class ControTweet
 */
@WebServlet("/ControTweet")
public class ControTweet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	/**
	 * Untuk memproses string agar sesuai dengan ASCII.
	 * Menghapus semua karakter yang mengandung nilai integer
	 * lebih dari sama dengan 128
	 * @param s String yang akan diproses
	 * @return String yang sudah difilter
	 */
	private String processString(String s)
	{
		String str = "";
		for(int i = 0; i < s.length();i++)
			if ((int)s.charAt(i)<128)
				str+=s.charAt(i);
		return str;
	}
	
	/**
	 * Proses utama untuk pemprosesan data query dan pattern
	 * @param _patternbagus Vector yang berisi String pattern frasa baik
	 * @param _patternjelek Vector yang berisi String pattern frasa buruk
	 * @param _query Query yang akan dicari
	 * @param _type tipe pencarian (KMP/BM)
	 * @return ArrayList of String HTML untuk masing-masing tweets baik, buruk, dan netral
	 */
	public ArrayList<String> ModelTwitter(Vector<String> _patternbagus, Vector<String> _patternjelek, String _query, String _type)
	{	
		Vector<Status> statusbagus = new Vector<Status>();
		Vector<Status> statusjelek = new Vector<Status>();
		Vector<Status> statusnetral = new Vector<Status>();
		try {
			ConfigurationBuilder cb = new ConfigurationBuilder();
			cb.setDebugEnabled(true)
			  .setOAuthConsumerKey("XS4Zf7jRBw1GKlAy8Tqg")
			  .setOAuthConsumerSecret("AAZUACuFlxkoKevEQXHQvDQMf7tMsHRpbtdWhvGAWu4")
			  .setOAuthAccessToken("77255827-AJFmFFyrtKAIu2VQfwMSzi1FONa76vG4EULCkvZdB")
			  .setOAuthAccessTokenSecret("KM8S0d3R7qeT9tf6ekXMvVqQuyH6HwPGMVht2vcSeBX9h");
			TwitterFactory tf = new TwitterFactory(cb.build());
			Twitter twitter = tf.getInstance();
		    Query query = new Query(_query);
		    query.count(10);
		    QueryResult result;
		    int count;
			result = twitter.search(query);
			for (Status status : result.getTweets()) {
		        count =0;
				for(String s:_patternbagus){
					if(_type.equals("kmp")){
					if(KMP(processString(status.getText().toLowerCase()), s.toLowerCase()))count++;}
					else if(bmMatch(processString(status.getText().toLowerCase()), s.toLowerCase())>-1)count++;
				}
				for(String s:_patternjelek){
					if(_type.equals("kmp")){
					if(KMP(processString(status.getText().toLowerCase()), s.toLowerCase()))count--;}
					else if(bmMatch(processString(status.getText().toLowerCase()), s.toLowerCase())>-1)count--;
				}
				if(count>0)statusbagus.add(status);
				else if(count<0) statusjelek.add(status);
				else statusnetral.add(status);
		    }
			ArrayList<String> values = new ArrayList<String>();
			String strbagus ="";
			String strnetral ="";
			String strjelek ="";
			for(Status st : statusbagus){
				strbagus += "<a target='_blank' href='https://twitter.com/" + st.getUser().getScreenName() + "/statuses/" + st.getId() + "'><div class='tweets'><div><img class='gambar' src='"+st.getUser().getProfileImageURL()+"' /><h3>" + st.getUser().getName() + " </h3><h3 class='id'>@"+st.getUser().getScreenName()+"</h3></div><p>" + st.getText()+"</p><h6>" + st.getCreatedAt().toString() + "</h6></div></a>";
			}
			for(Status st : statusjelek){
				strjelek+= "<a target='_blank' href='https://twitter.com/" + st.getUser().getScreenName() + "/statuses/" + st.getId() + "'><div class='tweets'><div><img class='gambar' src='"+st.getUser().getProfileImageURL()+"' /><h3>" + st.getUser().getName() + " </h3><h3 class='id'>@"+st.getUser().getScreenName()+"</h3></div><p>" + st.getText()+"</p><h6>" + st.getCreatedAt().toString() + "</h6></div></a>";
			}
			for(Status st : statusnetral){
				strnetral+= "<a target='_blank' href='https://twitter.com/" + st.getUser().getScreenName() + "/statuses/" + st.getId() + "'><div class='tweets'><div><img class='gambar' src='"+st.getUser().getProfileImageURL()+"' /><h3>" + st.getUser().getName() + " </h3><h3 class='id'>@"+st.getUser().getScreenName()+"</h3></div><p>" + st.getText()+"</p><h6>" + st.getCreatedAt().toString() + "</h6></div></a>";
			}
			if (strbagus.equals(""))
				strbagus="No result.";
			else
				strbagus = String.valueOf(statusbagus.size()) + " tweets found." + strbagus;
			
			if (strjelek.equals(""))
				strjelek="No result.";
			else
				strjelek = String.valueOf(statusjelek.size()) + " tweets found." + strjelek;
			
			if (strnetral.equals(""))
				strnetral="No result.";
			else
				strnetral = String.valueOf(statusnetral.size()) + " tweets found." + strnetral;
			
			values.add(strbagus);
			values.add(strjelek);
			values.add(strnetral);
			return values;
		} catch (TwitterException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Memproses String menjadi Vector yang terdiri dari sekumpulan
	 * String yang merupakan substring dari 0 hingga n
	 * @param str
	 * @return Vector<String> yang merupakan substring dari 0 hingga n
	 */
    public static Vector<String> prefixes(String str){
        Vector<String> prefix = new Vector<String>();
        String prefs = "";
        for (int i = 0; i<str.length(); i++){
            prefix.add(prefs);
            prefs = prefs + str.charAt(i);
        }
        return prefix;
    }
    
    /**
	 * Memproses String menjadi Vector yang terdiri dari sekumpulan
	 * String yang merupakan substring dari n hingga panjang String
	 * @param str
	 * @return Vector<String> yang merupakan substring dari n hingga panjang String
	 */
    public static Vector<String> suffixes(String str){
        Vector<String> suffix = new Vector<String>();
        String suffs = "";
        for (int i = str.length()-1; i>=0; i--){
            suffix.add(suffs);
            suffs = str.charAt(i)+suffs;
        }
        return suffix;
    }
    
    /**
     * Mencari String yang merupakan Suffix dari pAksen dan Prefix dari P yang terpanjang 
     * @param pAksen diproses untuk suffix
     * @param P diproses untuk prefix
     * @return Strinng
     */
    public static String LongestSuffixThatAlsoPrefix(String pAksen, String P){
        String longest = "";
        Vector<String> suffixpAksen = new Vector<String>();
        suffixpAksen = suffixes(pAksen);
        Vector<String> prefixP = new Vector<String>();
        prefixP = prefixes(P);
        for (int i = 0; i<suffixpAksen.size(); i++){
            for (int j = 0; j<prefixP.size(); j++){
                if (suffixpAksen.get(i).equals(prefixP.get(j))){
                    if (prefixP.get(j).length()>longest.length()){
                        longest = prefixP.get(j);
                    }
                }
            }
        }
        return longest;
    }
    
    /**
     * Mencari letak karakter x pada String string
     * @param x karakter yang ingin dicari
     * @param string String yang akan dicari letak karakternya
     * @return index terakhit letak karakter x pada string
     */
    public static int L(char x, String string){
        int n = string.length();
        int i = n-1;
        while ((i>=0)&&(string.charAt(i)!=x)){
            i--;
        }
        if ((i>=0)&&(string.charAt(i)==x)){
            return i;
        }
        else return -1;
    }
    
    /**
     * Menyelesaikan proses pattern Matching dengan KMP
     * @param string String yang akan dicari
     * @param pattern Pattern yang akan dicari
     * @return apakah pattern merupakan substring dari string
     */
    public static boolean KMP(String string, String pattern){
        int n = pattern.length();
        int N = string.length();
        int posP0diT = 0;
        int i = posP0diT;
        int j = 0;
        while ((j<n)&&(i<N)){
            String pAksen = "";
            while ((j<n)&&(i<N)&&(pattern.charAt(j)==string.charAt(i))){
                pAksen = pAksen + pattern.charAt(j);
                j++;
                i++;
            }
            if ((j<n)&&(i<N)&&(pattern.charAt(j)!=string.charAt(i))){
                if (pAksen.length()==0){
                    posP0diT = posP0diT+ 1;
                    i = posP0diT;
                    j = 0;
                }
                else {
                    String L = LongestSuffixThatAlsoPrefix(pAksen, pattern);
                    posP0diT = posP0diT + (pAksen.length()-L.length());
                    i = posP0diT;
                    j = 0;
                }
            }
        }
        if (j>=n) return true;
        else return false;
    }

    /**
     * Menyelesaikan proses pattern Matching dengan bmMatch
     * @param text String yang akan dicari
     * @param pattern Pattern yang akan dicari
     * @return apakah pattern merupakan substring dari text
     */
    public int bmMatch(String text, String pattern)
    {
    	int last[] = buildLast(pattern);
    	int n = text.length();
    	int m = pattern.length();
    	int i = m-1;
    	
    	if (i>(n-1))
    		return -1;
    	
    	int j = m-1;
    	do {
    		if (pattern.charAt(j)==text.charAt(i))
    			if (j==0)
    				return i;
    			else {
    				i--;
    				j--;
    			}
    		else {
    			int lo = last[text.charAt(i)];
    			i = i + m - Math.min(j,i+lo);
    			j = m - 1;
    		}
    	} while (i<= n-1);
    	return -1;
    }

    /**
     * Menentukan indeks terakhir masing-masing karakter ASCII pada pattern
     * @param pattern String yang akan diproses
     * @return Array of integer yang merupakan index terakhir tiap karakter. -1 berarti tidak ditemukan
     */
    public int[] buildLast(String pattern)
    {
    	int last[] = new int[128];
    	
    	for (int i = 0; i < 128;i++)
    		last[i] = -1;
    	
    	for (int i = 0; i < pattern.length(); i++)
    		last[pattern.charAt(i)] = i;
    	
    	return last;
    }
    
    /**
     * Default constructor. 
     */
    public ControTweet() {
    	super();
    }

	/**
	 * Pemprosesan request POST. Mengembalikan JSON berupa HTML untuk kolom positif, negatif, dan netral
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Vector<String> baik = new Vector<String>();
    	Vector<String> buruk = new Vector<String>();
		String data_positif = request.getParameter("positif");
		String data_negatif = request.getParameter("negatif");
		String data_subjek = request.getParameter("subjek");
		String data_type = request.getParameter("type");
		System.out.println("positif : " + data_positif);
		System.out.println("negatif : " + data_negatif);
		System.out.println("subjek : " + data_subjek);
		System.out.println("type : " + data_type);
		String[] temp = data_positif.split(";");
		for(String s:temp){
    		baik.add(s);
    	}
		temp = data_negatif.split(";");
		for(String s:temp){
    		buruk.add(s);
    	}
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		ArrayList<String> x = ModelTwitter(baik, buruk, data_subjek, data_type);
		JSONObject data = new JSONObject();
		data.put("baik", x.get(0));
		data.put("buruk", x.get(1));
		data.put("netral", x.get(2));
		System.out.println(data);
        out.print(data);
	}
}
