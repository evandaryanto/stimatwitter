<!DOCTYPE html>
<html>
<head>
<link rel='stylesheet' type="text/css" href='${pageContext.request.contextPath}/css/style.css'></link>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.js"></script>
<link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.ico">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Tweety - do more with Twitter</title>
</head>
<body>
	<!-- HALAMAN FORM UTAMA -->
	<div class='input-vert' id='input-container'>
		<form class='input' id='datainput' enctype="multipart/form-data">
			<img id='logo' src='${pageContext.request.contextPath}/img/logo.png' />
			<label>Sentimen Positif</label>
				<div id='text_pos_box' class='input_container'>
					<input type="text" name='positif' id='in_pos'/>
					<span class='changetofile' id='changetofile-pos' title="Take from our database"></span>
				</div>
			<label>Sentimen Negatif</label>
				<div id='text_neg_box' class='input_container'>
					<input type="text" name='negatif' id='in_neg'/>
					<span class='changetofile' id='changetofile-neg' title="Take from our database"></span>
				</div>
			<label>Keyword</label>
				<input type='text' name='query' id='in_que'/>
			<label>Algoritma</label>
			<select name="algorithm" id='in_opt'>
				<option value="bm">Boyer-Moore</option>
				<option value="kmp">Knuth Morris Pratt</option>
			</select>
			<button type='submit'>Tweety!</button>
		</form>
	</div>
	
	<!-- HALAMAN LOADING -->
	<div class='loading-vert hidden' id='loading'>
		<img id='logo' src='${pageContext.request.contextPath}/img/logo.png' />
		<div class='loading'><img src='${pageContext.request.contextPath}/img/spinner.gif' /> Processing...</div>
	</div>
	
	<!-- HALAMAN HASIL PENCARIAN -->
	<div class='content-vert hidden' id='content'>
		<div class='3div div1'>
			<h1>POSITIVE TWEETS</h1>
			<div id='colbaik'></div>
		</div>
		<div class='3div div2'>
			<h1>NEGATIVE TWEETS</h1>
			<div id='colburuk'></div>
		</div>
		<div class='3div div3'>
			<h1>NEUTRAL TWEETS</h1>
			<div id='colnetral'></div>
		</div>
	</div>
	
	<!-- HALAMAN ABOUT -->
	<div class='horizontal hidden' id='about'>
		<h2>WHAT DO YOU THINK ABOUT</h2>
		<img class='logo' src='${pageContext.request.contextPath}/img/logo.png' />
		<p><span class='tweety'>Tweety</span> adalah sebuah portal yang dapat mengekstrasi tweets pada twitter. Tidak hanya itu, tweets ini bisa dikelompokan menjadi tweet positif, negatif, dan netral. Frase-frase yang menyebabkan positif atau negatif dapat ditentukan oleh user ataupun melalui database kami.</p>
		<img src='${pageContext.request.contextPath}/img/actor.png' />
		<p><span class='tweety'>Tweety</span> dibuat oleh mahasiswa Teknik Informatika 2011 Institut Teknologi Bandung, yaitu Ignatius Evan Daryanto (kiri), F.X. Christian Gunardi (tengah), dan Destra Bintang Perkasa (kanan) untuk memenuhi tugas besar III IF2211-Strategi Algoritma.</p>
	</div>
	
	<!-- HEADER (LOGO DAN MENU UTAMA) -->
	<div class='header'>
		<img src='${pageContext.request.contextPath}/img/logo_dark.png' />
		<div class='menu' id='menu_search'>SEARCH</div>
		<div class='menu' id='menu_result'>RESULT</div>
		<div class='menu' id='menu_about'>ABOUT</div>
	</div>

<script type="text/javascript">
var lockmenu = false; // boolean : apakah menu dapat digunakan atau tidak
var haveresult = false; // boolean : apakah user pernah melakukan pencarian atau tidak

//pemprosesan POST untuk form utama
$(document).on('submit','#datainput',function(e) {
	lockmenu = true;
    $('#input-container').fadeOut("slow",function(){
    	$('#loading').fadeIn("slow",function(){
    		$.post(<%="\""+request.getContextPath()+"/ControTweet\""%>, { "positif":$('#in_pos').val(),"negatif":$('#in_neg').val(),"subjek":$('#in_que').val(),"type":$('#in_opt').val(), }, function( data ) {
    			haveresult = true;
    			lockmenu = false;
    			$('#colbaik').html(data.baik);
    			$('#colburuk').html(data.buruk);
    			$('#colnetral').html(data.netral);
    			$('#loading').fadeOut("slow",function(){
  	    			$('#content').fadeIn("slow");
  		    	});
    	    }, "json");
    	});
    });
	return false;
});

// pemprosesan pengambilan frase dari database
$('#changetofile-pos').click(function() {
	jQuery.get("<%= request.getContextPath()%>/data/positif.txt", function(data){$("#in_pos").val(data)});
});
$('#changetofile-neg').click(function() {
	jQuery.get("<%= request.getContextPath()%>/data/negatif.txt", function(data){$("#in_neg").val(data)});
});

//fungsi yang akan menghide semua tampilan dan memunculkan tampilan yang diinginkan
function Show(show){
	$('#about').fadeOut("slow",function(){
		$('#input-container').fadeOut("slow",function(){
			$('#loading').fadeOut("slow",function(){
				$('#content').fadeOut("slow",function(){
					$(show).fadeIn("slow");
				});
			});
		});
	})
}

// Pemprosesan click menu search
$('#menu_search').click(function(){
	if (lockmenu) // saat menu sedang dilock
		alert("Proses sedang berjalan. Menu tidak dapat diakses"); // mengeluarkan alert
	else // saat menu tidak sedang dilock
		Show('#input-container');
});

//Pemprosesan click menu result
$('#menu_result').click(function(){
	if (lockmenu) // saat menu sedang dilock
		alert("Proses sedang berjalan. Menu tidak dapat diakses"); // mengeluarkan alert
	else if (haveresult) // saat menu tidak sedang dilock dan pernah melakukan pencarian
		Show('#content'); // mengeluarkan hasil pencarian terakhir
	else
		alert("Anda belum pernah melakukan pencarian"); // mengeluarkan alert
});

//Pemprosesan click menu about
$('#menu_about').click(function(){
	if (lockmenu) // saat menu sedang dilock
		alert("Proses sedang berjalan. Menu tidak dapat diakses"); // mengeluarkan alert
	else // saat menu tidak sedang dilock
		Show("#about"); // mengeluarkan halaman about
});
</script>
</body>
</html>